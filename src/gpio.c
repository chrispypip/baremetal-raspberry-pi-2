/*****
* @file gpio.c
* @author Christopher Piparo
*
* Implementation file for using the GPIO peripheral on the Raspberry Pi 2
*****/

#include "gpio.h"
#include <inttypes.h>

// Peripheral register access
static volatile GPIO *const gpio = (GPIO *)GPIO_BASE;

// Initialize a GPIO pin
void gpio_pin_init(GPIO_PIN pin, GPIO_FS func)
{
	// Determine which register to use to select the function of the proper pin
	uint32_t *fs_reg = &((uint32_t *)gpio)[pin/NUM_PINS_PER_FSEL];

	// Set the function of the specified pin
	*fs_copy &= ~(GPIO_FS_CLR << ((pin%NUM_PINS_PER_FSEL)*NUM_BITS_PER_FUNC));
	*fs_copy |= (func << ((pin%NUM_PINS_PER_FSEL)*NUM_BITS_PER_FUNC));
}

// Set the value of a GPIO pin
void gpio_pin_set_val(GPIO_PIN pin, GPIO_VAL val)
{
	if((val == GPIO_VAL_HI) || (val == GPIO_VAL_ON)) // Set pin high
	{
		// Determine which register to use to set the proper pin
		switch(pin/NUM_PINS_PER_REG)
		{
			case 0:
				gpio->GPIO_REG_GPSET0 = (1<<pin);
				break;
			case 1:
				gpio->GPIO_REG_GPSET1 = (1<<(pin-NUM_PINS_PER_REG));
				break;
			default:
				break;
		}
	}
	else if((val == GPIO_VAL_LO) || (val == GPIO_VAL_OFF)) // Set pin low
	{
		// Determine which register to use to clear the proper pin
		switch(pin/NUM_PINS_PER_REG)
		{
			case 0:
				gpio->GPIO_REG_GPCLR0 = (1<<pin);
				break;
			case 1:
				gpio->GPIO_REG_GPCLR1 = (1<<(pin-NUM_PINS_PER_REG));
				break;
			default:
				break;
		}
	}
}

// Get the value of a GPIO pin
GPIO_VAL gpio_pin_get_val(GPIO_PIN pin)
{
	GPIO_VAL result = GPIO_VAL_UNKNOWN;

	// Determine which register to use to fetch the proper pin's value
	switch(pin/NUM_PINS_PER_REG)
	{
		case 0:
			result = (gpio->GPIO_REG_GPLEV0 & (1<<pin)) >> pin;
			break;
		case 1:
			result = (gpio->GPIO_REG_GPLEV1 & (1<<(pin-NUM_PINS_PER_REG))) >> (pin-NUM_PINS_PER_REG);
			break;
		default:
			break;
	}

	return result;
}

// Set the pull-up or pull-down resistoer of a GPIO pin
void gpio_pin_set_pud(GPIO_PIN pin, GPIO_PUD pud)
{
	// Enable the pull-up/pull-down resistor
	gpio->GPIO_REG_GPPUD = pud;
	
	// Wait 150 clock cycles (see BCM2835 datasheet)
	for(volatile int i=0; i<PUD_CLK_DELAY; i++);

	// Determine which register to use to set proper value
	switch(pin/NUM_PINS_PER_REG)
	{
		case 0:
			// Refer to datasheet for an explanation of these steps.
			// Essentially, the PUD clock must be enabled and then
			// Disabled after 150 clock cycles so that the resistor
			// can be initialized
			gpio->GPIO_REG_GPPUDCLK0 = (1<<pin);
			for(volatile int i=0; i<PUD_CLK_DELAY; i++);
			gpio->GPIO_REG_GPPUDCLK0 = 0;
			break;
		case 1:
			// Refer to datasheet for an explanation of these steps.
			// See 'case 0' for brief
			gpio->GPIO_REG_GPPUDCLK1 = (1<<(pin-NUM_PINS_PER_REG));
			for(volatile int i=0; i<PUD_CLK_DELAY; i++);
			gpio->GPIO_REG_GPPUDCLK1 = 0;
			break;
		default:
			break;
	}
}

