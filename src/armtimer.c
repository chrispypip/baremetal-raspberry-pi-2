/*****
* @file armtimer.c
* @author Christopher Piparo
*
* Implementation file for using the ARM timer on the Raspberry Pi 2
*****/

#include "armtimer.h"
#include <inttypes.h>

// Peripheral register access
static volatile ARMTIMER *const armtimer = (ARMTIMER *)ARMTIMER_BASE;

// Initailize the ARM timer
void armtimer_init(ARMTIMER_BIT_MODE mode, uint32_t us)
{
	armtimer->ARMTIMER_REG_LOAD = us;
	armtimer->ARMTIMER_REG_CTRL |= (mode<<ARMTIMER_CTRL_COUNTER_MODE);
}

// Start the ARM timer
void armtimer_start()
{
	armtimer->ARMTIMER_REG_CTRL |= ARMTIMER_CTRL_TIMER_EN;
}

// Stop the ARM timer
void armtimer_stop()
{
	armtimer->ARMTIMER_REG_CTRL &= ~(ARMTIMER_CTRL_TIMER_EN);

}

// Enable the ARM timer interrupt
void armtimer_irq_en()
{
	armtimer->ARMTIMER_REG_CTRL |= ARMTIMER_CTRL_IRQ_EN;
}

// Set the ARM timer prescaler value
void armtimer_set_prescaler(ARMTIMER_PSC psc)
{
	armtimer->ARMTIMER_REG_CTRL |= (psc<<ARMTIMER_CTRL_PSC_MODE);
}

// Clear the ARM timer intterupt flag
void armtimer_irq_clr()
{
	armtimer->ARMTIMER_REG_IRQ_CLR = 1;
}
