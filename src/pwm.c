/*****
* @file pwm.c
* @author Christopher Piparo
*
* Implementation file for using the PWM peripheral on the Raspberry Pi 2
*****/

#include "pwm.h"
#include "systimer.h"
#include <inttypes.h>

// Clock frequency supplied to the PWM
#define PWM_CLK_FREQ	19200000UL

// Peripheral register access
static volatile PWM		*const pwm     = (PWM *)PWM_BASE;
static volatile PWM_CLK *const pwm_clk = (PWM_CLK *)PWM_CLK_BASE;

static uint32_t period  = 24000; // Initial period of the PWM signal. Used for frequency calculation
static uint16_t clk_div = 16;	// Initiial clock divider of the PWM signal

// Initialize a PWM signal
void pwm_init(PWM_CHN chn)
{
	// For some reason, a 110us-ish delay is needed here
	systimer_delay_us(110);

	// Setup default values
	pwm_set_mode(chn, PWM_MODE_BAL);
	pwm_set_range(chn, period);
	pwm_set_pol(chn, PWM_POL_LOW0);
	pwm_div_clk(clk_div);
}

// Set the mode to use for the PWM channel
void pwm_set_mode(PWM_CHN chn, PWM_MODE mode)
{
	// Determine which register to use for the proper channel
	if(chn)
	{
		// Determine whether to clear or set the MS enable register to set the mode
		if(mode)
		{
			pwm->PWM_REG_CTL |= PWM_CTL_MS_EN2;
		}
		else
		{
			pwm->PWM_REG_CTL &= ~PWM_CTL_MS_EN2;
		}
	}
	else
	{
		// Determine whether to clear or set the MS enable register to set the mode
		if(mode)
		{
			pwm->PWM_REG_CTL |= PWM_CTL_MS_EN1;
		}
		else
		{
			pwm->PWM_REG_CTL &= ~PWM_CTL_MS_EN1;
		}
	}
}

// Set the range of a PWM signal
void pwm_set_range(PWM_CHN chn, uint32_t range)
{
	// Set new period
	period = range;

	// Determine which register to use to set the range of the proper channel
	if(chn)
	{
		pwm->PWM_REG_RNG2 = range;
	}
	else
	{
		pwm->PWM_REG_RNG1 = range;
	}

	// For some reason, a 10us-ish delay is needed here
	systimer_delay_us(10);
}

// Set the polarity of a PWM signal
void pwm_set_pol(PWM_CHN chn, PWM_POL pol)
{
	// Determine which register to use to set the polarity of the proper channel
	if(chn)
	{
		// Determine whether to clear or set the polarity register to set the proper polarity
		if(pol)
		{
			pwm->PWM_REG_CTL |= PWM_CTL_REV_POL2;
		}
		else
		{
			pwm->PWM_REG_CTL &= ~PWM_CTL_REV_POL2;
		}
	}
	else
	{
		// Determine whether to clear or set the polarity register to set the proper polarity
		if(pol)
		{
			pwm->PWM_REG_CTL |= PWM_CTL_REV_POL1;
		}
		else
		{
			pwm->PWM_REG_CTL &= ~PWM_CTL_REV_POL1;
		}
	}
}

// Set the duty cycle of a PWM signal
void pwm_set_dc(PWM_CHN chn, uint32_t dc)
{
	// Calculate the value to set in the PWM data register
	uint32_t data = period / (100/dc);

	// Determine which register to use to set the data of the proper channel
	if(chn)
	{
		pwm->PWM_REG_DAT2 = data;
	}
	else
	{
		pwm->PWM_REG_DAT1 = data;
	}
}

// Play a specified frequency on a PWM signal
void pwm_play_freq(PWM_CHN chn, uint32_t freq)
{
	// Calculate the new period
	period = PWM_CLK_FREQ / clk_div / freq;

	// Setup the PWM to play the frequency
	pwm_set_range(chn, period);
	pwm_set_dc(chn, 50);
}

// Divide the PWM clock
void pwm_div_clk(uint16_t divisor)
{
	uint32_t pwm_ctl;
	divisor &= 4095;
	clk_div = divisor;

	// Get the current PWM control register settings, as it neeeds
	// to be set to 0 in order to set up the clock
	pwm_ctl = pwm->PWM_REG_CTL;
	pwm->PWM_REG_CTL = 0;

	// Access the PWM clock manager
	pwm_clk->PWM_CLK_REG_CTL = CLK_MNGR_PSWD | 0x01;
	systimer_delay_us(110);

	// Wait for the PWM clock controller to not be busy
	while(pwm_clk->PWM_CLK_REG_CTL & PWM_CLK_CTL_BUSY);
	systimer_delay_us(1);

	// Set up the clock divider. Refer to BCM2835 datasheet
	pwm_clk->PWM_CLK_REG_DIV = CLK_MNGR_PSWD | (divisor<<PWM_CLK_DIV_DIV);
	pwm_clk->PWM_CLK_REG_CTL = CLK_MNGR_PSWD | 0x11;

	// Re-set the PWM control register
	pwm->PWM_REG_CTL = pwm_ctl;
}

// Start a PWM signal
void pwm_start(PWM_CHN chn)
{
	// Determine which register to use to enable the proper channel
	if(chn)
	{
		pwm->PWM_REG_CTL |= PWM_CTL_EN2;
	}
	else
	{
		pwm->PWM_REG_CTL |= PWM_CTL_EN1;
	}
}
