/*****
* @file interrupts.c
* @author Christopher Piparo
*
* Implementation file for using the interrupts on the Raspberry Pi 2
*****/

#include "interrupts.h"
#include "armtimer.h"
#include "gpio.h"
#include "systimer.h"
#include <inttypes.h>

// Peripheral register access
static volatile IRQ_CONTROLLER *const irq_controller = (IRQ_CONTROLLER *)IRQ_CONTROLLER_BASE;

// Initialize a basic interrupt
void irq_controller_basic_irq_init(IRQ_CONTROLLER_BASIC_IRQ irq)
{
	irq_controller->IRQ_REG_EN_BASIC_IRQ |= 1<<irq;
}

// Initialize a non-basic interrupt
void irq_controller_irq_init(IRQ_CONTROLLER_IRQ irq)
{
	switch(irq/IRQ_CONTROLLER_NUM_PINS_PER_REG)
	{
		case 0:
			irq_controller->IRQ_REG_EN_IRQ1 = (1<<irq);
			break;
		case 1:
			irq_controller->IRQ_REG_EN_IRQ2 = (1<<(irq-IRQ_CONTROLLER_NUM_PINS_PER_REG));
			break;
		default:
			break;
	}
}

// Poll the pending flag of a basic interrupt
uint8_t irq_controller_poll_basic_irq_pend(IRQ_CONTROLLER_BASIC_IRQ irq)
{
	return (irq_controller->IRQ_REG_BASIC_PEND & (1<<irq)) >> irq;
}

// Poll the pending flag of a non-basic interrupts
uint8_t irq_controller_poll_irq_pend(IRQ_CONTROLLER_IRQ irq)
{
	uint8_t result = 0;

	switch(irq/32)
	{
		case 0:
			result = (irq_controller->IRQ_REG_PEND1 & (1<<irq)) >> irq;
			break;
		case 1:
			result = (irq_controller->IRQ_REG_PEND2 & (1<<(irq-32))) >> (irq-32);
			break;
		default:
			break;
	}

	return result;
}


// List of interrupt handlers. Can be used in ANY file.
// Simply cut and paste where desired. 
void __attribute__((interrupt("ABORT"))) _reset_(void)
{

}

void __attribute__((interrupt("UNDEF"))) undefined_instruction(void)
{

}

void __attribute__((interrupt("SWI"))) supervisor_call(void)
{

}

void __attribute__((interrupt("ABORT"))) prefetch_abort(void)
{

}

void __attribute__((interrupt("ABORT"))) data_abort(void)
{

}

void __attribute__((interrupt("IRQ"))) interrupt_handler(void)
{

}

void __attribute__((optimize("O0"))) fast_interrupt_handler(void)
{

}
