+/*****
+* @file systimer.c
+* @author Christopher Piparo
+*
+* Implementation file for using the systimer timer for a delay on the Raspberry Pi 2
+*****/
+

#include "systimer.h"
#include <inttypes.h>

// Peripheral register access
static volatile SYSTIMER *const systimer = (SYSTIMER *)SYSTIMER_BASE;

// Create a delay in microseconds using the system timer
void systimer_delay_us(uint32_t delay)
{
	// Get the current value in the timer register
	volatile uint32_t ts = systimer->SYSTIMER_REG_COUNTLO;

	// Wait for specified amount of time
	while((systimer->SYSTIMER_REG_COUNTLO - ts) < delay);
}

// Create a delay in milliseconds using the system timer
void systimer_delay_ms(uint32_t delay)
{
	// Get the current value in the timer register
	volatile uint32_t ts = systimer->SYSTIMER_REG_COUNTLO;

	// Wait for specified amount of time
	while((systimer->SYSTIMER_REG_COUNTLO - ts) < (delay*US_TO_MS));
}
