/*****
* @file aux.c
* @author Christopher Piparo
*
* Implementation file for using the auxilliary on the Raspberry Pi 2
*
* NOTE: Only the mini UART is implemented at this time
*****/

#include "aux.h"
#include "gpio.h"
#include <inttypes.h>

// Peripheral register access
static volatile AUX *const aux = (AUX *)AUX_BASE;

// Prototye to setup the GPIO to use the mini UART
static void aux_mu_setup_gpio();

// Frequency of the system. Used to set baud
#define SYS_FREQ 250000000

// Initialize the mini UART
void aux_mu_init(uint32_t baud, AUX_MU_BIT_MODE mode)
{
	// enable mini UART to access registers
	aux->AUX_REG_ENABLES |= AUX_ENB_EN_MU;
	//aux->AUX_REG_MU_IER = 0;
	//aux->AUX_REG_MU_CNTL = 0;

	// set bit mode
	aux->AUX_REG_MU_LCR |= mode;

	//aux->AUX_REG_MU_MCR = 0;
	//aux->AUX_REG_MU_IER = 0;
	// Clear receive and transmit FIFOs
	aux->AUX_REG_MU_IIR = (AUX_MU_IIR_CLR_RX_FIFO | AUX_MU_IIR_CLR_TX_FIFO);

	// set baud rate
	aux->AUX_REG_MU_BAUD = (SYS_FREQ/(8*baud))-1;

	// setup GPIO pins to enable UART communication
	aux_mu_setup_gpio();

	// enable transmitter
	aux->AUX_REG_MU_CNTL |= AUX_MU_CNTL_TX_EN;
}

// Write a character to the mini UART
void aux_mu_write(char c)
{
	// Wait for the transmittion buffer to be empty
	while(!(aux->AUX_REG_MU_LSR & AUX_MU_LSR_TX_EMPTY));

	// Write the character to the mini UART
	aux->AUX_REG_MU_IO = c;
}

// Setup the GPIO to use the mini UART
void aux_mu_setup_gpio()
{
	// GPIO pins 14 and 15 alternate function's 5 are used for mini UART
	gpio_pin_init(GPIO_PIN14, GPIO_FS_ALT5);
	gpio_pin_init(GPIO_PIN15, GPIO_FS_ALT5);

	// Make sure the pull-up/pull-down resistors are disable
	gpio_pin_set_pud(GPIO_PIN14, GPIO_PUD_OFF);
	gpio_pin_set_pud(GPIO_PIN15, GPIO_PUD_OFF);
}
