extern int __bss_start__;
extern int __bss_end__;

extern void kernel_main(unsigned r0, unsigned r1, unsigned atags);

void _cstartup(unsigned r0, unsigned r1, unsigned r2)
{
	int *bss = &__bss_start__;
	int *bss_end = &__bss_end__;

	while(bss < bss_end)
	{
		*bss++ = 0;
	}

	kernel_main(r0, r1, r2);

	for(;;);
}
