/*****
* @file aux.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the auxilliar peripheral
* of the Raspbery Pi 2.
*
* NOTE: Only the mini UART is implemented at this time.
*****/

// Compiler guard
#ifndef AUX_H
#define AUX_H

#include <inttypes.h>

// Peripheral base
#define AUX_BASE				0x3F215000UL

// AUX IRQ enable register bits
#define AUX_IRQ_EN_MU_IRQ		(1<<0)
#define AUX_IRQ_EN_SPI1_IRQ		(1<<1)
#define AUX_IRQ_EN_SPI2_IRQ		(1<<2)

// AUX IRQ enable register bits
#define AUX_ENB_EN_MU			(1<<0)
#define AUX_ENB_EN_SPI1			(1<<1)
#define AUX_ENB_EN_SPI2			(1<<2)

// AUX mini UART IRQ enable register bits
#define AUX_MU_IER_EN_RX_IRQ	(1<<0)
#define AUX_MU_IER_EN_TX_IRQ	(1<<1)

// AUX mini UART IRQ status register bits
#define AUX_MU_IIR_IRQ_PEND		(1<<0)
#define AUX_MU_IIR_CLR_RX_FIFO	(1<<1)
#define AUX_MU_IIR_CLR_TX_FIFO	(1<<2)

// AUX mini UART access register bits
#define AUX_MU_LCR_BREAK		(1<<6)
#define AUX_MU_LCR_DLAB			(1<<7)

// AUX mini UART RTS status register bits
#define AUX_MU_MCR_RTS			(1<<1)

// AUX mini UART data status register bits
#define AUX_MU_LSR_DATA_READY	(1<<0)
#define AUX_MU_LSR_RX_OVERRUN	(1<<1)
#define AUX_MU_LSR_TX_EMPTY		(1<<5)
#define AUX_MU_LSR_TX_IDLE		(1<<6)

// AUX mini UART CTS status register bits
#define AUX_MU_MSR_CTS			(1<<5)

// AUX mini UART features register bits
#define AUX_MU_CNTL_RX_EN		(1<<0)
#define AUX_MU_CNTL_TX_EN		(1<<1)
#define AUX_MU_CNTL_EN_RTS		(1<<2)
#define AUX_MU_CNTL_EN_CTS		(1<<3)
#define AUX_MU_CNTL_RTS_LV		(1<<6)
#define AUX_MU_CNTL_CTS_LV		(1<<7)

// AUX mini UART status register bits
#define AUX_MU_STAT_SYM_AV		(1<<0)
#define AUX_MU_STAT_SPACE_AV	(1<<1)
#define AUX_MU_STAT_RX_IDLE		(1<<2)
#define AUX_MU_STAT_TX_IDLE		(1<<3)
#define AUX_MU_STAT_RX_OVERRUN	(1<<4)
#define AUX_MU_STAT_TX_FULL		(1<<5)
#define AUX_MU_STAT_RTS			(1<<6)
#define AUX_MU_STAT_CTS			(1<<7)
#define AUX_MU_STAT_TX_EMPTY	(1<<8)
#define AUX_MU_STAT_TX_DONE		(1<<9)

// Bit mode for mini UART to use (7 or 8)
typedef enum
{
	AUX_MU_7BIT_MODE,
	AUX_MU_8BIT_MODE = 3
} AUX_MU_BIT_MODE;

// Auxilliar mini UART register layout
typedef struct
{
	uint32_t AUX_REG_IRQ;
	uint32_t AUX_REG_ENABLES;
	uint32_t unused0[((0x40-0x04)/4)-1];

	uint32_t AUX_REG_MU_IO;
	uint32_t AUX_REG_MU_IER;
	uint32_t AUX_REG_MU_IIR;
	uint32_t AUX_REG_MU_LCR;
	uint32_t AUX_REG_MU_MCR;
	uint32_t AUX_REG_MU_LSR;
	uint32_t AUX_REG_MU_MSR;
	uint32_t AUX_REG_MU_SCRATCH;
	uint32_t AUX_REG_MU_CNTL;
	uint32_t AUX_REG_MU_STAT;
	uint32_t AUX_REG_MU_BAUD;
} AUX;

/*
* Initialize the mini UART
*
* @param baud - The baud rate to set
* @param mode - Select with bit mode to use
*/
void aux_mu_init(uint32_t baud, AUX_MU_BIT_MODE mode);

/*
* Write a character to the mini UART
*
* @param c - The character to write
*/
void aux_mu_write(char c);

#endif // End compiler guard
