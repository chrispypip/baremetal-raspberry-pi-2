/*****
* @file gpio.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the GPIO peripheral
* of the Raspbery Pi 2.
*****/

#ifndef GPIO_H
#define GPIO_H

#include <inttypes.h>

// Clock cycles to dealy when setting PUD
#define PUD_CLK_DELAY		150

// Peripheral base address
#define GPIO_BASE			0x3F200000UL

// Values to determine register bits to set/clr
#define GPIO_FS_CLR			7
#define NUM_PINS_PER_FSEL	10
#define NUM_BITS_PER_FUNC	3
#define NUM_PINS_PER_REG	32

// GPIO pins
typedef enum
{
	GPIO_PIN0,
	GPIO_PIN1,
	GPIO_PIN2,
	GPIO_PIN3,
	GPIO_PIN4,
	GPIO_PIN5,
	GPIO_PIN6,
	GPIO_PIN7,
	GPIO_PIN8,
	GPIO_PIN9,
	GPIO_PIN10,
	GPIO_PIN11,
	GPIO_PIN12,
	GPIO_PIN13,
	GPIO_PIN14,
	GPIO_PIN15,
	GPIO_PIN16,
	GPIO_PIN17,
	GPIO_PIN18,
	GPIO_PIN19,
	GPIO_PIN20,
	GPIO_PIN21,
	GPIO_PIN22,
	GPIO_PIN23,
	GPIO_PIN24,
	GPIO_PIN25,
	GPIO_PIN26,
	GPIO_PIN27,
	GPIO_PIN28,
	GPIO_PIN29,
	GPIO_PIN30,
	GPIO_PIN31,
	GPIO_PIN32,
	GPIO_PIN33,
	GPIO_PIN34,
	GPIO_PIN35,
	GPIO_PIN36,
	GPIO_PIN37,
	GPIO_PIN38,
	GPIO_PIN39,
	GPIO_PIN40,
	GPIO_PIN41,
	GPIO_PIN42,
	GPIO_PIN43,
	GPIO_PIN44,
	GPIO_PIN45,
	GPIO_PIN46,
	GPIO_PIN47,
	GPIO_PIN48,
	GPIO_PIN49,
	GPIO_PIN50,
	GPIO_PIN51,
	GPIO_PIN52,
	GPIO_PIN53
} GPIO_PIN;

// GPIO function select
typedef enum
{
	GPIO_FS_INPUT,
	GPIO_FS_OUTPUT,
	GPIO_FS_ALT5,
	GPIO_FS_ALT4,
	GPIO_FS_ALT0,
	GPIO_FS_ALT1,
	GPIO_FS_ALT2,
	GPIO_FS_ALT3
} GPIO_FS;

// GPIO pin levels
typedef enum
{
	GPIO_VAL_LO,
	GPIO_VAL_HI,
	GPIO_VAL_OFF,
	GPIO_VAL_ON,
	GPIO_VAL_UNKNOWN
} GPIO_VAL;

// GPIO pull-up/down modes
typedef enum
{
	GPIO_PUD_OFF,
	GPIO_PUD_DOWN,
	GPIO_PUD_UP
} GPIO_PUD;

// GPIO register structure
typedef struct
{
	uint32_t		GPIO_REG_GPFSEL0;
	uint32_t 		GPIO_REG_GPFSEL1;
	uint32_t 		GPIO_REG_GPFSEL2;
	uint32_t 		GPIO_REG_GPFSEL3;
	uint32_t 		GPIO_REG_GPFSEL4;
	uint32_t 		GPIO_REG_GPFSEL5;
	const uint32_t 	unused0;
	uint32_t 		GPIO_REG_GPSET0;
	uint32_t 		GPIO_REG_GPSET1;
	const uint32_t 	unused1;
	uint32_t 		GPIO_REG_GPCLR0;
	uint32_t 		GPIO_REG_GPCLR1;
	const uint32_t 	unused2;
	uint32_t 		GPIO_REG_GPLEV0;
	uint32_t 		GPIO_REG_GPLEV1;
	const uint32_t 	unused3;
	uint32_t 		GPIO_REG_GPEDS0;
	uint32_t 		GPIO_REG_GPEDS1;
	const uint32_t 	unused4;
	uint32_t 		GPIO_REG_GPREN0;
	uint32_t 		GPIO_REG_GPREN1;
	const uint32_t 	unused5;
	uint32_t 		GPIO_REG_GPFEN0;
	uint32_t 		GPIO_REG_GPFEN1;
	const uint32_t 	unused6;
	uint32_t 		GPIO_REG_GPHEN0;
	uint32_t 		GPIO_REG_GPHEN1;
	const uint32_t 	unused7;
	uint32_t 		GPIO_REG_GPLEN0;
	uint32_t 		GPIO_REG_GPLEN1;
	const uint32_t 	unused8;
	uint32_t 		GPIO_REG_GPAREN0;
	uint32_t 		GPIO_REG_GPAREN1;
	const uint32_t 	unused9;
	uint32_t 		GPIO_REG_GPAFEN0;
	uint32_t 		GPIO_REG_GPAFEN1;
	const uint32_t 	unused10;
	uint32_t 		GPIO_REG_GPPUD;
	uint32_t 		GPIO_REG_GPPUDCLK0;
	uint32_t 		GPIO_REG_GPPUDCLK1;
} GPIO;

/*
* Initialize a GPIO pin
*
* @param pin - The GPIO pin to set up
* @param func - The function of 'pin' to set
*/
void gpio_pin_init(GPIO_PIN pin, GPIO_FS func);

/*
* Set the value of a GPIO pin. Must be initialized first
*
* @param pin - The pin whose value to set
* @param val - The value to set the pin to
*/
void gpio_pin_set_val(GPIO_PIN pin, GPIO_VAL val);

/*
* Get the current value of a GPIO pin
*
* @param pin - The GPIO pin to get the value of
*
* @return The value of 'pin'
*/
GPIO_VAL gpio_pin_get_val(GPIO_PIN pin);

/*
* Set the pull-up or pull-down resister for a GPIO pin
*
* @param pin - The pin to set the pull-up/pull-down resistor of
* @param pud - Pull-up or pull-down or neither
*/
void gpio_pin_set_pud(GPIO_PIN pin, GPIO_PUD pud);

#endif // End compiler guard
