/*****
* @file systimer.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the system timer peripheral
* on the Raspberry Pi 2
*****/

// Compiler guard
#ifndef SYSTIMER_H
#define SYSTIMER_H

#include <inttypes.h>

// Peripheral base
#define SYSTIMER_BASE	0x3F003000UL

#define US_TO_MS		1000

// Systimer register structure
typedef struct
{
	uint32_t SYSTIMER_REG_STATUS;
	uint32_t SYSTIMER_REG_COUNTLO;
	uint32_t SYSTIMER_REG_COUNTHI;
	uint32_t SYSTIMER_REG_COMPARE0;
	uint32_t SYSTIMER_REG_COMPARE1;
	uint32_t SYSTIMER_REG_COMPARE2;
	uint32_t SYSTIMER_REG_COMPARE3;
} SYSTIMER;

/*
* Create a delay in microseconds using the system timer of the Raspberry Pi 2
*
* @param delay - The number of microseconds to delay
*/
void systimer_delay_us(uint32_t delay);

/*
* Create a delay in milliseconds using the system timer of the Raspberry Pi 2
*
* @param dealy - The number of milliseconds to delay
*/
void systimer_delay_ms(uint32_t delay);

#endif // End compiler guard
