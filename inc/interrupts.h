/*****
* @file interrupts.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the interrupts on the
* Raspberry Pi 2.
*****/

// Compiler guard
#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#include <inttypes.h>

// Peripheral base
#define IRQ_CONTROLLER_BASE	0x3F00B200

#define IRQ_CONTROLLER_NUM_PINS_PER_REG 32

// Basic interrupts table
typedef enum
{
	IRQ_BASIC_IRQ_ARMTIMER,
	IRQ_BASIC_IRQ_MAILBOX,
	IRQ_BASIC_IRQ_DOORBELL0,
	IRQ_BASIC_IRQ_DOORBELL1,
	IRQ_BASIC_IRQ_GPU0_HALT,
	IRQ_BASIC_IRQ_GPU1_HALT,
	IRQ_BASIC_IRQ_ILLEGAL1,
	IRQ_BASIC_IRQ_ILLEGAL0
} IRQ_CONTROLLER_BASIC_IRQ;

// Other interrupts table
typedef enum
{
	IRQ_IRQ_AUX = 29,
	IRQ_IRQ_I2CX_SPI_SLV = 43,
	IRQ_IRQ_PWA0 = 45,
	IRQ_IRQ_PWA1 = 46,
	IRQ_IRQ_SMI = 48,
	IRQ_IRQ_GPIO0 = 49,
	IRQ_IRQ_GPIO1 = 50,
	IRQ_IRQ_GPIO2 = 51,
	IRQ_IRQ_GPIO3 = 52,
	IRQ_IRQ_I2C = 53,
	IRQ_IRQ_SPI = 54,
	IRQ_IRQ_PCM = 55,
	IRQ_IRQ_UART = 57
} IRQ_CONTROLLER_IRQ;

// Interrupt register structure
typedef struct
{
	uint32_t IRQ_REG_BASIC_PEND;
	uint32_t IRQ_REG_PEND1;
	uint32_t IRQ_REG_PEND2;
	uint32_t IRQ_REG_FIQ_CTRL;
	uint32_t IRQ_REG_EN_IRQ1;
	uint32_t IRQ_REG_EN_IRQ2;
	uint32_t IRQ_REG_EN_BASIC_IRQ;
	uint32_t IRQ_REG_DIS_IRQ1;
	uint32_t IRQ_REG_DIS_IRQ2;
	uint32_t IRQ_REG_DIS_BASIC_IRQ;
} IRQ_CONTROLLER;

/*
* Initialize one of the basic interrupts
*
* @param irq - The basic interrupt to enable
*/
void irq_controller_basic_irq_init(IRQ_CONTROLLER_BASIC_IRQ irq);

/*
* Enable one of the non-basic interrupts
*
* @param irq - The intterupt to enable
*/
void irq_controller_irq_init(IRQ_CONTROLLER_IRQ irq);

/*
* Poll the basic interrupt pending register to determine if the interrupt fired
*
* @param irq - The basic interrupt to poll the value of
*
* @return The value of the pending status of 'irq'
*/
uint8_t irq_controller_poll_basic_irq_pend(IRQ_CONTROLLER_BASIC_IRQ irq);

/*
* Poll the non-basic intterupt pending register to determine if the interrupt fired
*
* @param irq - The interrupt to poll the value of
*
* @return The value of the pending status of 'irq'
*/
uint8_t irq_controller_poll_irq_pend(IRQ_CONTROLLER_IRQ irq);

#endif // End compiler guard
