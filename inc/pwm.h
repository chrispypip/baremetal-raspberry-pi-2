/*****
* @file pwm.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the PWM peripheral
* of the Raspbery Pi 2.
*****/

#ifndef PWM_H
#define PWM_H

#include <inttypes.h>

// Peripheral base
#define PWM_BASE			0x3F20C000
#define PWM_CLK_BASE		0x3F1010A0

// Clock manager password
#define CLK_MNGR_PSWD		(0x5A<<24)

// PWM control register bits
#define PWM_CTL_EN1			(1<<0)
#define PWM_CTL_SERIAL1		(1<<1)
#define PWM_CTL_RPT_LAST1	(1<<2)
#define PWM_CTL_SILENCE1	(1<<3)
#define PWM_CTL_REV_POL1	(1<<4)
#define PWM_CTL_USE_FIFO1	(1<<5)
#define PWM_CTL_CLR_FIFO1	(1<<6)
#define PWM_CTL_MS_EN1		(1<<7)
#define PWM_CTL_EN2			(1<<8)
#define PWM_CTL_SERIAL2		(1<<9)
#define PWM_CTL_RPT_LAST2	(1<<10)
#define PWM_CTL_SILENCE2	(1<<11)
#define PWM_CTL_REV_POL2	(1<<12)
#define PWM_CTL_USE_FIFO2	(1<<13)
#define PWM_CTL_MS_EN2		(1<<15)

// PWM status register bits
#define PWM_STA_FIFO_FULL	(1<<0)
#define PWM_STA_FIFO_EMPTY	(1<<1)
#define PWM_STA_FIFO_WR_ERR	(1<<2)
#define PWM_STA_FIFO_RD_ERR	(1<<3)
#define PWM_STA_CHN1_GAP	(1<<4)
#define PWM_STA_CHN2_GAP	(1<<5)
#define PWM_STA_CHN3_GAP	(1<<6)
#define PWM_STA_CHN4_GAP	(1<<7)
#define PWM_STA_BUS_ERR		(1<<8)
#define PWM_STA_CHN1_TX		(1<<9)
#define PWM_STA_CHN2_TX		(1<<10)
#define PWM_STA_CHN3_TX		(1<<11)
#define PWM_STA_CHN4_TX		(1<<12)

// PWM DMA register bits
#define PWM_DMAC_DRQ		0
#define PWM_DMAC_PANIC		8
#define PWM_DMAC_EN			(1<<31)

// PWM clock manager control register bits
#define PWM_CLK_CTL_OSC		(1<<0)
#define PWM_CLK_CTL_DBG0	(2<<0)
#define PWM_CLK_CTL_DBG1	(3<<0)
#define PWM_CLK_CTL_PLLA	(4<<0)
#define PWM_CLK_CTL_PLLC	(5<<0)
#define PWM_CLK_CTL_PLLD	(6<<0)
#define PWM_CLK_CTL_HDMI	(7<<0)
#define PWM_CLK_CTL_EN		(1<<4)
#define PWM_CLK_CTL_KILL	(1<<5)
#define PWM_CLK_CTL_BUSY	(1<<7)
#define PWM_CLK_CTL_FLIP	(1<<8)
#define PWM_CLK_CTL_MASH1	(1<<9)
#define PWM_CLK_CTL_MASH2	(2<<9)
#define PWM_CLK_CTL_MASH3	(3<<9)

// PWM clock control divisor register bits
#define PWM_CLK_DIV_FRACT	0
#define PWM_CLK_DIV_DIV		12

// PWM channels
typedef enum
{
	PWM_CHN1,
	PWM_CHN2
} PWM_CHN;

// PWM polarity
typedef enum
{
	PWM_POL_LOW0,
	PWM_POL_LOW1
} PWM_POL;

// PWM transmissions
typedef enum
{
	PWM_MODE_BAL,
	PWM_MODE_MS
} PWM_MODE;

// PWM register structure
typedef struct
{
	uint32_t PWM_REG_CTL;
	uint32_t PWM_REG_STA;
	uint32_t PWM_REG_DMAC;
	uint32_t unused0;

	uint32_t PWM_REG_RNG1;
	uint32_t PWM_REG_DAT1;
	uint32_t PWM_REG_FIF1;
	uint32_t unused1;

	uint32_t PWM_REG_RNG2;
	uint32_t PWM_REG_DAT2;
} PWM;

// PWM clock manager register structure
typedef struct
{
	uint32_t PWM_CLK_REG_CTL;
	uint32_t PWM_CLK_REG_DIV;
} PWM_CLK;

/*
* Initialize one of the PWM channels
*
* @param chn - The PWM channel to initialize
*/
void pwm_init(PWM_CHN chn);

/*
* Set which PWM mode to use for a channel. Channel must be intialized first
*
* @param chn - The PWM channel whose mode is to be changed
* @param mode - The PWM mode to set 'chn' to
*/
void pwm_set_mode(PWM_CHN chn, PWM_MODE mode);

/*
* Set the range value of a PWM channel
*
* @param chn - The PWM channel whose range is to be set (refer to BCM2835 documentation)
* @param range - The range to set
*/
void pwm_set_range(PWM_CHN chn, uint32_t range);

/*
* Set the polarity of a PWM channel
*
* @param chn - The PWM channel whose polarity is to be changed
* @param pol - The polarity to set 'chn' to
*/
void pwm_set_pol(PWM_CHN chn, PWM_POL pol);

/*
* Set the duty cycle of a PWM channel
*
* @param chn - The PWM channel whose duty cycle is to be changed
* @param dc - The duty cycle to set as a percent
*/
void pwm_set_dc(PWM_CHN chn, uint32_t dc);

/*
* Play a frequency on a PWM channel
*
* @param chn - The PWM channel to play a frequency on
* @freq - The frequency to play in Hz
*/
void pwm_play_freq(PWM_CHN chn, uint32_t freq);

/*
* Divide the clock supplied to the PWM
*
* @param divisor - The number to divide the clock by
*/
void pwm_div_clk(uint16_t divisor);

/*
* Start the PWM. Must be initialized first
*
* @param chn - The PWM channel to start
*/
void pwm_start(PWM_CHN chn);

#endif // End compiler guard
