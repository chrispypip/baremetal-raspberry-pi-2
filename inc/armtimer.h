/*****
* @file armtimer.h
* @author Christopher Piparo
*
* Header file for operating the registers and functions of the ARM timer peripheral
* of the Raspbery Pi 2.
*****/

// Compiler guard
#ifndef ARMTIMER_H
#define ARMTIMER_H

#include <inttypes.h>

// Peripheral base
#define ARMTIMER_BASE				0x3F00B400UL

// Timer control register bits
#define ARMTIMER_CTRL_COUNTER_MODE	1
#define ARMTIMER_CTRL_PSC_MODE		2
#define ARMTIMER_CTRL_IRQ_EN		(1<<5)
#define ARMTIMER_CTRL_TIMER_EN		(1<<7)
#define ARMTIMER_CTRL_DEBUG_HALT	(1<<8)
#define ARMTIMER_CTRL_FRC_EN		(1<<9)
#define ARMTIMER_CTRL_FRC_PSC_SHIFT	16

// Timer raw IRQ register bits
#define RAW_IRQ_IRQ_PEND			(1<<0)

// Timer masked IRQ register bits
#define RAW_IRQ_IRQ_ASSERT			(1<<0)

// Timer counter bit mode
typedef enum
{
	ARMTIMER_16BIT_MODE,
	ARMTIMER_32BIT_MODE
} ARMTIMER_BIT_MODE;

// Timer interrupt mode
typedef enum
{
	ARMTIMER_IRQ_DIS,
	ARMTIMER_IRQ_EN
} ARMTIMER_IRQ_MODE;

// Prescaler modes
typedef enum
{
	ARMTIMER_PSC1,
	ARMTIMER_PSC16,
	ARMTIMER_PSC256
} ARMTIMER_PSC;

// ARMTIMER register structure
typedef struct
{
	uint32_t ARMTIMER_REG_LOAD;
	uint32_t ARMTIMER_REG_VALUE;
	uint32_t ARMTIMER_REG_CTRL;
	uint32_t ARMTIMER_REG_IRQ_CLR;
	uint32_t ARMTIMER_REG_RAW_IRQ;
	uint32_t ARMTIMER_REG_MASKED_IRQ;
	uint32_t ARMTIMER_REG_RELOAD;
	uint32_t ARMTIMER_REG_PREDIVIDER;
	uint32_t ARMTIMER_REG_FRC;
} ARMTIMER;

/*
* Initialize the ARM timer.
*
* @param mode - The bit mode to set (16 or 32)
* @param us - The number of microseconds to count
*/
void armtimer_init(ARMTIMER_BIT_MODE mode, uint32_t us);

/*
* Start the ARM timer. Must be intialized first
*/
void armtimer_start();

/*
* Stop the ARM timer.
*/
void armtimer_stop();

/*
* Enable the interrupt for the ARM timer. Must be initailized first
*/
void armtimer_irq_en();

/*
* Set the prescaler value for the ARM timer. Must be initialized first
*
* @param psc - The prescaler value to set
*/
void armtimer_set_prescaler(ARMTIMER_PSC psc);

/*
* Clear the intterupt flag for the ARM timer
*/
void armtimer_irq_clr();

#endif // End compiler guard
